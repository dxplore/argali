#!/usr/bin/env python

# Copyright 2020 Jan Van Winkel <jan.van_winkel@dxplore.eu>
#
# SPDX-License-Identifier: Apache-2.0
'''
Ecppll tool wrapper script
'''

import pathlib
import subprocess
import sys

from fusesoc.capi2.generator import Generator


class EcppllGenerator(Generator):
    '''
    Ecppll tool wrapper script
    '''

    def run(self):
        module_name = self.config.get('module_name', 'pll')
        file_name = self.config.get('file_name', "pll.v")
        clkin_name = self.config.get('clkin_name', 'clkin')
        clkin_freq = self.config.get('clkin_freq', 25)
        clkout0_name = self.config.get('clkout0_name', 'clkout0')
        clkout0_freq = self.config.get('clkout0_freq', 50)
        clkout1_enable = self.config.get('clkout1_enable', False)
        clkout1_name = self.config.get('clkout1_name', 'clkout1')
        clkout1_freq = self.config.get('clkout1_freq', 50)
        phase1 = self.config.get('phase1', 0)
        clkout2_enable = self.config.get('clkout2_enable', False)
        clkout2_name = self.config.get('clkout2_name', 'clkout2')
        clkout2_freq = self.config.get('clkout2_freq', 50)
        phase2 = self.config.get('phase2', 0)
        clkout3_enable = self.config.get('clkout3_enable', False)
        clkout3_name = self.config.get('clkout3_name', 'clkout3')
        clkout3_freq = self.config.get('clkout3_freq', 50)
        phase3 = self.config.get('phase3', 0)
        highres = self.config.get('highres', False)
        dynamic = self.config.get('dynamic', False)
        reset = self.config.get('reset', False)
        standby = self.config.get('standby', False)
        feedback_clkout = self.config.get('feedback_clkout', 0)
        internal_feedback = self.config.get('internal_feedback', False)
        internal_feedback_wake = self.config.get('internal_feedback_wake',
                                                 False)

        args = ['ecppll']
        args.extend(['--module', module_name])
        args.extend(['--file', file_name])
        args.extend(['--clkin_name', clkin_name])
        args.extend(['--clkin', str(clkin_freq)])
        args.extend(['--clkout0_name', clkout0_name])
        args.extend(['--clkout0', str(clkout0_freq)])
        if clkout1_enable:
            args.extend(['--clkout1_name', clkout1_name])
            args.extend(['--clkout1', str(clkout1_freq)])
            args.extend(['--phase1', str(phase1)])
        if clkout2_enable:
            args.extend(['--clkout2_name', clkout2_name])
            args.extend(['--clkout2', str(clkout2_freq)])
            args.extend(['--phase2', str(phase2)])
        if clkout3_enable:
            args.extend(['--clkout3_name', clkout3_name])
            args.extend(['--clkout3', str(clkout3_freq)])
            args.extend(['--phase3', str(phase3)])
        if highres:
            args.append('--highres')
        if dynamic:
            args.append('--dynamic')
        if reset:
            args.append('--reset')
        if standby:
            args.append('--standby')
        args.extend(['--feedback_clkout', str(feedback_clkout)])
        if internal_feedback:
            args.extend('--internal_feedback')
        if internal_feedback_wake:
            args.extend('--internal_feedback_wake')

        res = subprocess.run(args, stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)

        if res.returncode != 0:
            print("{res.stdout}")
            sys.exit(res.returncode)

        self.add_files([{file_name: {'file_type': 'verilogSource'}}])


g = EcppllGenerator()
g.run()
g.write()
