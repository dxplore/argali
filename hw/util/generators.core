CAPI=2:

# Copyright 2020 - jan.van_winkel@dxplore.eu
#
# SPDX-License-Identifier: Apache-2.0

name: dxplore:argali:generators
description: "Argali specific generators"

generators:
  tlgen:
    interpreter: python3
    command: tlgen.py

  ecppll:
    interpreter: python3
    command: ecppll.py
    description: Generate a parameterized verilog module for ECP5
    usage: |
      The ecppll generator is a simple wrapper around the ecppll command

      Parameters:
        module_name (str):              PLL module name
                                          (default: pll)
        file_name (str):                Output filename
                                          (default: ecppll.v)
        clkin_name (str):               Input clock signal name
                                          (default: clkin)
        clkin_freq (int):               Input clock frequency in MHz
                                          (default: 25 MHz)
        clkout0_name (str):             Primary Output(0) signal name
                                          (default: clkout0)
        clkout0_freq (int):             Primary Output(0) frequency in MHz
                                          (default: 50 MHz)
        clkout1_enable (bool):          Secondary Output(1) enable
                                          (default: false)
        clkout1_name (str):             Secondary Output(1) signal name
                                          (default: clkout1)
        clkout1_freq (int):             Secondary Output(1) frequency in MHz
                                          (default: 50 MHz)
        phase1 (int):                   Secondary Output(1) phase in degrees
                                          (default:  0)
        clkout2_enable (bool):          Secondary Output(2) enable
                                          (default: false)
        clkout2_name                    Secondary Output(2) signal name
                                          (default: clkout2)
        clkout2_freq (int):             Secondary Output(2) frequency in MHz
                                          (default: 50 MHz)
        phase2 (int):                   Secondary Output(2) phase in degrees
                                          (default:  0)
        clkout3_enable (bool):          Secondary Output(3) enable
                                          (default: false)
        clkout3_name                    Secondary Output(3) signal name
                                          (default: clkout3)
        clkout3_freq (int):             Secondary Output(3) frequency in MHz
                                          (default: 50 MHz)
        phase3 (int):                   Secondary Output(3) phase in degrees
                                          (default:  0)
        highres (bool):                 Use secondary PLL output for higher
                                        frequency resolution
                                          (default: false)
        dynamic (bool):                 Use dynamic clock control
                                          (default: false)
        reset (bool):                   Enable reset input
                                          (default: false)
        standby (bool):                 Enable standby input
                                          (default: false)
        feedback_clkout (int):          Use Nth Output as feedback signal
                                          (default: 0)
        internal_feedback (bool):       Use internal feedback
                                        (instead of external)
                                          (default: false)
        internal_feedback_wake (bool):  Wake internal feedback
                                          (default: false)
