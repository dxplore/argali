// Copyright 2020 - jan.van_winkel@dxplore.eu
//
// SPDX-License-Identifier: Apache-2.0

module tlul_ibex #(
  parameter bit                 PMPEnable        = 1'b0,
  parameter int unsigned        PMPGranularity   = 0,
  parameter int unsigned        PMPNumRegions    = 4,
  parameter int unsigned        MHPMCounterNum   = 0,
  parameter int unsigned        MHPMCounterWidth = 40,
  parameter bit                 RV32E            = 1'b0,
  parameter ibex_pkg::rv32m_e   RV32M            = ibex_pkg::RV32MFast,
  parameter ibex_pkg::rv32b_e   RV32B            = ibex_pkg::RV32BNone,
  parameter ibex_pkg::regfile_e RegFile          = ibex_pkg::RegFileFPGA,
  parameter bit                 BranchTargetALU  = 1'b0,
  parameter bit                 WritebackStage   = 1'b0,
  parameter bit                 ICache           = 1'b0,
  parameter bit                 ICacheECC        = 1'b0,
  parameter bit                 BranchPredictor  = 1'b0,
  parameter bit                 DbgTriggerEn     = 1'b0,
  parameter bit                 SecureIbex       = 1'b0,
  parameter int unsigned        DmHaltAddr       = 32'h1A110800,
  parameter int unsigned        DmExceptionAddr  = 32'h1A110808,
  parameter int unsigned        BootAddr         = 32'h10000000
) (
  input clk_i,
  input rst_ni,

  output tlul_pkg::tl_h2d_t  tl_instr_o,
  input  tlul_pkg::tl_d2h_t  tl_instr_i,

  output tlul_pkg::tl_h2d_t  tl_data_o,
  input  tlul_pkg::tl_d2h_t  tl_data_i,

  input debug_req_i,

  input irq_timer_i,
  input irq_external_i,
  input irq_software_i
);

  localparam int FifoPassthrough = 0;
  localparam int FifoDepth = 2;

  logic        instr_req;
  logic        instr_gnt;
  logic        instr_rvalid;
  logic [31:0] instr_addr;
  logic [31:0] instr_rdata;

  logic        data_req;
  logic        data_gnt;
  logic        data_rvalid;
  logic        data_we;
  logic [3:0]  data_be;
  logic [31:0] data_addr;
  logic [31:0] data_wdata;
  logic [31:0] data_rdata;

  tlul_pkg::tl_h2d_t tl_instr_ibex2fifo;
  tlul_pkg::tl_d2h_t tl_instr_fifo2ibex;

  tlul_pkg::tl_h2d_t tl_data_ibex2fifo;
  tlul_pkg::tl_d2h_t tl_data_fifo2ibex;

  ibex_core #(
    .PMPEnable(PMPEnable),
    .PMPGranularity(PMPGranularity),
    .PMPNumRegions(PMPNumRegions),
    .MHPMCounterNum(MHPMCounterNum),
    .MHPMCounterWidth(MHPMCounterWidth),
    .RV32E(RV32E),
    .RV32M(RV32M),
    .RV32B(RV32B),
    .RegFile(RegFile),
    .BranchTargetALU(BranchTargetALU),
    .WritebackStage(WritebackStage),
    .ICache(ICache),
    .ICacheECC(ICacheECC),
    .BranchPredictor(BranchPredictor),
    .DbgTriggerEn(DbgTriggerEn),
    .SecureIbex(SecureIbex),
    .DmHaltAddr(DmHaltAddr),
    .DmExceptionAddr(DmExceptionAddr)
  ) u_core (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .test_en_i(1'b0),
    .ram_cfg_i('b0),

    .hart_id_i      (32'b0),
    .boot_addr_i    (BootAddr),

    .instr_req_o    (instr_req),
    .instr_gnt_i    (instr_gnt),
    .instr_rvalid_i (instr_rvalid),
    .instr_addr_o   (instr_addr),
    .instr_rdata_i  (instr_rdata),
    .instr_err_i    (1'b0),

    .data_req_o     (data_req),
    .data_gnt_i     (data_gnt),
    .data_rvalid_i  (data_rvalid),
    .data_we_o      (data_we),
    .data_be_o      (data_be),
    .data_addr_o    (data_addr),
    .data_wdata_o   (data_wdata),
    .data_rdata_i   (data_rdata),
    .data_err_i     (1'b0),

    .irq_software_i (irq_software_i),
    .irq_timer_i    (irq_timer_i),
    .irq_external_i (irq_external_i),
    .irq_fast_i     (15'b0),
    .irq_nm_i       (1'b0),

    .debug_req_i    (debug_req_i),
    .crash_dump_o   (),

    .fetch_enable_i (1'b1),
    .alert_minor_o  (),
    .alert_major_o  (),
    .core_sleep_o   ()
  );

  /* Break long paths through tilelink cross-bar by introducing 2 stages of pipelining on both
  * instruction and data path
   */

  tlul_fifo_sync #(
    .ReqPass(FifoPassthrough),
    .RspPass(FifoPassthrough),
    .ReqDepth(FifoDepth),
    .RspDepth(FifoDepth)
  ) fifo_instr (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .tl_h_i(tl_instr_ibex2fifo),
    .tl_h_o(tl_instr_fifo2ibex),
    .tl_d_o(tl_instr_o),
    .tl_d_i(tl_instr_i),
    .spare_req_i(1'b0),
    .spare_req_o(),
    .spare_rsp_i(1'b0),
    .spare_rsp_o()
  );

  tlul_fifo_sync #(
    .ReqPass(FifoPassthrough),
    .RspPass(FifoPassthrough),
    .ReqDepth(FifoDepth),
    .RspDepth(FifoDepth)
  ) fifo_data (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .tl_h_i(tl_data_ibex2fifo),
    .tl_h_o(tl_data_fifo2ibex),
    .tl_d_o(tl_data_o),
    .tl_d_i(tl_data_i),
    .spare_req_i(1'b0),
    .spare_req_o(),
    .spare_rsp_i(1'b0),
    .spare_rsp_o()
  );

  /* Actual tilelink cross-bar host access controllers for both data and instruction path
   */
  tlul_adapter_host #(
    .MAX_REQS(2)
  ) tlul_instr (
    .clk_i      (clk_i),
    .rst_ni     (rst_ni),
    .req_i      (instr_req),
    .type_i     (tlul_pkg::InstrType),
    .gnt_o      (instr_gnt),
    .addr_i     (instr_addr),
    .we_i       (1'b0),
    .wdata_i    (32'b0),
    .be_i       (4'hF),
    .valid_o    (instr_rvalid),
    .rdata_o    (instr_rdata),
    .err_o      (),
    .tl_o       (tl_instr_ibex2fifo),
    .tl_i       (tl_instr_fifo2ibex),
    .intg_err_o ()
  );

  tlul_adapter_host #(
    .MAX_REQS(2)
  ) tlul_data (
    .clk_i       (clk_i),
    .rst_ni      (rst_ni),
    .req_i       (data_req),
    .type_i      (tlul_pkg::DataType),
    .gnt_o       (data_gnt),
    .addr_i      (data_addr),
    .we_i        (data_we),
    .wdata_i     (data_wdata),
    .be_i        (data_be),
    .valid_o     (data_rvalid),
    .rdata_o     (data_rdata),
    .err_o       (),
    .tl_o        (tl_data_ibex2fifo),
    .tl_i        (tl_data_fifo2ibex),
    .intg_err_o  ()
  );

endmodule
