// Copyright 2020 - jan.van_winkel@dxplore.eu
//
// SPDX-License-Identifier: Apache-2.0

module tlul_ram #(
  parameter int AddressWidth = 14,
  parameter int DataWidth = 32,
  parameter int Depth = 'h4000, // 64k bytes = 16k (depth) * 4 byes (32-bit)
  parameter int DataBitsPerMask = 8,

  parameter MemInitFile = ""
) (
  input clk_i,
  input rst_ni,
  input tlul_pkg::tl_h2d_t tl_i,
  output tlul_pkg::tl_d2h_t tl_o
);

  logic                    mem_req;
  logic                    mem_we;
  logic [AddressWidth-1:0] mem_addr;
  logic [DataWidth-1:0]    mem_wdata;
  logic [DataWidth-1:0]    mem_wmask;
  logic [DataWidth-1:0]    mem_rdata;
  logic                    mem_rvalid;

  always_ff @(posedge clk_i or negedge rst_ni) begin
    if (!rst_ni) begin
      mem_rvalid <= 1'b0;
    end else begin
      mem_rvalid <= mem_req & ~mem_we;
    end
  end

  tlul_adapter_sram #(
    .SramAw(AddressWidth),
    .SramDw(DataWidth)
  ) u_tl_adapter_ram_main (
    .clk_i       (clk_i),
    .rst_ni      (rst_ni),
    .tl_i        (tl_i),
    .tl_o        (tl_o),
    .en_ifetch_i (1'b1), // TODO: Always allow instruction fetch for now
    .req_o       (mem_req),
    .gnt_i       (1'b1),
    .we_o        (mem_we),
    .addr_o      (mem_addr),
    .wdata_o     (mem_wdata),
    .wmask_o     (mem_wmask),
    .intg_error_o(),
    .rdata_i     (mem_rdata),
    .rvalid_i    (mem_rvalid),
    .rerror_i    (2'b0)
  );

  prim_ram_1p #(
    .Width(DataWidth),
    .Depth(Depth),
    .MemInitFile(MemInitFile)
  ) u_ram (
    .clk_i     (clk_i),
    .req_i     (mem_req),
    .write_i   (mem_we),
    .addr_i    (mem_addr),
    .wdata_i   (mem_wdata),
    .wmask_i   (mem_wmask),
    .rdata_o   (mem_rdata),
    .cfg_i     ('b0)
  );

endmodule
