/* Copyright 2020 - jan.van_winkel@dxplore.eu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

`verilator_config

lint_off -rule WIDTH -file "*/rtl/top_argali_basic.sv" -match "*scanmode_i*"

/* Based on rule from Ibex repo */
lint_off -rule UNOPTFLAT -file "*/rtl/ibex_cs_registers.sv" -match "*u_core.cs_registers_i.mie_q*"
lint_off -rule UNOPTFLAT -file "*/rtl/ibex_csr.sv" -match "*u_core.cs_registers_i.u_mie_csr.rdata_q*"

/* instr_new_id and instr_done_wb are only used if RVFI enabled */
lint_off -rule UNUSED -file "*/rtl/ibex_core.sv" -match "*instr_new_id*"
lint_off -rule UNUSED -file "*/rtl/ibex_core.sv" -match "*instr_done_wb*"

/* tlul host adapter queue support */
lint_off -rule WIDTH -file "*/rtl/tlul_adapter_host.sv" -match "*source_q*"

/* Lower 2 bits are not used as bus word size is 32-bit */
lint_off -rule UNUSED -file "*/rtl/tlul_adapter_host.sv" -match "*addr_i*1:0*"

/* Ignore unused tlul bus bits */
lint_off -rule UNUSED -file "*/rtl/tlul_adapter_host.sv" -match "*tl_i*66:50,17:2*"

/* Single request bus is combinatorial and doe not use clk and rst*/
lint_off -rule UNUSED -file "*/rtl/tlul_adapter_host.sv" -match "*clk_i*"
lint_off -rule UNUSED -file "*/rtl/tlul_adapter_host.sv" -match "*rst_ni*"

/* Only bit 1 of rerror_i is actually used */
lint_off -rule UNUSED -file "*/rtl/tlul_adapter_sram.sv" -match "*rerror_i*0*"

lint_off -rule WIDTH -file "*/rtl/tlul_ram.sv" -match "*en_ifetch_i*"

/* rspfifo_wready is only assued in a assertion */
lint_off -rule UNUSED -file "*/rtl/tlul_adapter_sram.sv" -match "*rspfifo_wready*"

/* Xbar is combinatorial and does not use clk and rst*/
lint_off -rule UNUSED -file "*/rtl/autogen/xbar_main.sv" -match "*clk_i*"
lint_off -rule UNUSED -file "*/rtl/autogen/xbar_main.sv" -match "*rst_ni*"

/* tlul fifo parameters */
lint_off -rule WIDTH -file "*/rtl/tlul_fifo_sync.sv" -match "*ReqPass*"
lint_off -rule WIDTH -file "*/rtl/tlul_fifo_sync.sv" -match "*RspPass*"
lint_off -rule WIDTH -file "*/rtl/tlul_fifo_sync.sv" -match "*ReqDepth*"
lint_off -rule WIDTH -file "*/rtl/tlul_fifo_sync.sv" -match "*RspDepth*"

/* Unused bits in tlul socket m1 */
lint_off -rule UNUSED -file "*/rtl/tlul_socket_m1.sv" -match "*arb_data*"

/* Comparison between 6 & 5 bits */
lint_off -rule WIDTH -file "*/rtl/uart_core.sv" -match "*Operator EQ expects 6 bits on the RHS*"
/* Unused bits in uart core */
lint_off -rule UNUSED -file "*/rtl/uart_core.sv" -match "*reg2hw*67:56*"

/* UART DPI waiver*/
lint_off -rule WIDTH -file "*/uartdpi.sv" -match "*uartdpi_read*generates 8 bits*"
lint_off -rule WIDTH -file "*/uartdpi.sv" -match "*uartdpi_can_read*generates 32 bits*"
lint_off -rule WIDTH -file "*/uartdpi.sv" -match "*rxsymbol*generates 8 bits*"
lint_off -rule UNUSED -file "*/uartdpi.sv" -match "*Bits of signal are not used:*c*[31:8]*"

/* rv_dm waiver */
lint_off -rule DECLFILENAME -file "*/dm_pkg.sv" -match "*Filename*dm_pkg*does not match PACKAGE name:*dm*"
lint_off -rule WIDTH -file "*/dm_csrs.sv" -match "*Operator ADD expects 8 bits on the RHS*"
lint_off -rule WIDTH -file "*/dm_csrs.sv" -match "*extraction of var*"
lint_off -rule WIDTH -file "*/dm_csrs.sv" -match "*Operator LT*"
lint_off -rule UNUSED -file "*/dm_csrs.sv" -match "*Bits of signal are not used*"
lint_off -rule UNUSED -file "*/dm_pkg.sv" -match "*Bits of signal are not used*"
lint_off -rule UNUSED -file "*/rv_dm.sv" -match "*Signal is not used*"
lint_off -rule UNUSED -file "*/dm_sba.sv" -match "*Signal is not used*"
lint_off -rule UNUSED -file "*/dm_mem.sv" -match "*Bits of signal are not used*"
lint_off -rule UNUSED -file "*/dmi_jtag.sv" -match "*Bits of signal are not used*"
lint_off -rule UNUSED -file "*/debug_rom.sv" -match "*Bits of signal are not used*"

/* TL Xbar waiver */
lint_off -rule UNOPTFLAT -file "*/rtl/tlul_socket_m1.sv" -match "*Signal unoptimizable: Feedback to clock or circular logic:*.dreq_fifo_i*"

/* PLIC waiver */
lint_off -rule WIDTH -file "*/rtl/rv_plic.sv" -match "*Operator ASSIGNW expects * bits on the Assign RHS*"
lint_off -rule WIDTH -file "*/rtl/rv_plic.sv" -match "*Output port connection *irq_id_o* expects 6 bits on the pin connection*"
lint_off -rule UNOPTFLAT -file "*/rtl/rv_plic_target.sv" -match "*Feedback to clock or circular logic*"

