/* Copyright 2020 Jan Van Winkel <jan.van_winkel@dxplore.eu>
 * SPDX-License-Identifier: Apache-2.0
 */
#include <iostream>

#include "verilated_toplevel.h"
#include "verilator_sim_ctrl.h"
#include "verilator_memutil.h"

int main(int argc, char **argv)
{
	top_argali_basic_verilator top;

	auto &simctrl = VerilatorSimCtrl::GetInstance();
	simctrl.SetTop(&top, &top.clk_i, &top.rst_ni,
		       VerilatorSimCtrlFlags::ResetPolarityNegative);
	simctrl.SetInitialResetDelay(100);

	VerilatorMemUtil memutil;
	memutil.RegisterMemoryArea("ram",
			"TOP.top_argali_basic_verilator.top.u_ram."
			"u_ram.gen_generic.u_impl_generic");
	simctrl.RegisterExtension(&memutil);

	std::cout << "Simulation of Argali Basic" << std::endl
		  << "==========================" << std::endl
		  << std::endl;

	/* Simctrl Exec returns a pair where the first element is the actual
	 * return value and the second value indicates if the simulation
	 * actually took place
	 */
	auto sim_exec_result = simctrl.Exec(argc, argv);
	return sim_exec_result.first;
}
