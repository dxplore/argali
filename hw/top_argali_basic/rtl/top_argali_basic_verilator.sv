
module top_argali_basic_verilator (
  // Clock and Reset
  input clk_i,
  input rst_ni
);

  /* UART */
  logic uart_rx;
  logic uart_tx;

  /* JTAG */
  logic tck;
  logic tms;
  logic trst_n;
  logic tdi;
  logic tdo;

  top_argali_basic top (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .uart_rx(uart_rx),
    .uart_tx(uart_tx),
    .tck_i(tck),
    .tms_i(tms),
    .trst_ni(trst_n),
    .tdi_i(tdi),
    .tdo_o(tdo)
  );

  uartdpi #(
    .BAUD('d921_600),
    .FREQ('d25_000_000),
    .NAME("uart0")
  ) u_uart (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .tx_o(uart_rx),
    .rx_i(uart_tx)
  );

  jtagdpi #(
    .Name("jtag0"),
    .ListenPort(44853)
  ) u_jtag (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .jtag_tck(tck),
    .jtag_tms(tms),
    .jtag_tdi(tdi),
    .jtag_tdo(tdo),
    .jtag_trst_n(trst_n),
    .jtag_srst_n()
  );

endmodule
