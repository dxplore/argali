
module top_argali_basic #(
  parameter MemInitFile = ""
) (
  input clk_i,
  input rst_ni,

  /* UART */
  input uart_rx,
  output uart_tx,

  /* JTAG */
  input tck_i,
  input tms_i,
  input trst_ni,
  input tdi_i,
  output tdo_o
);

  logic debug_req;
  logic ndmreset;

  logic sys_rst_n;

  logic irq_timer;

  logic [0:0] irq_external;
  logic [0:0] irq_software;
  logic [31:0] intr_src;

  logic intr_tx_watermark;
  logic intr_rx_watermark;
  logic intr_tx_empty;
  logic intr_rx_overflow;
  logic intr_rx_frame_err;
  logic intr_rx_break_err;
  logic intr_rx_timeout;
  logic intr_rx_parity_err;

  jtag_pkg::jtag_req_t jtag_req;
  jtag_pkg::jtag_rsp_t jtag_rsp;
  logic unused_tdo_oe_o;

  assign jtag_req.tck = tck_i;
  assign jtag_req.tms = tms_i;
  assign jtag_req.trst_n = trst_ni;
  assign jtag_req.tdi = tdi_i;
  assign tdo_o = jtag_rsp.tdo;
  assign unused_tdo_oe_o = jtag_rsp.tdo_oe;

  tlul_pkg::tl_h2d_t  tl_instr_o;
  tlul_pkg::tl_d2h_t  tl_instr_i;

  tlul_pkg::tl_h2d_t  tl_data_o;
  tlul_pkg::tl_d2h_t  tl_data_i;

  tlul_pkg::tl_h2d_t  tl_ram_i;
  tlul_pkg::tl_d2h_t  tl_ram_o;

  tlul_pkg::tl_h2d_t  tl_uart_i;
  tlul_pkg::tl_d2h_t  tl_uart_o;

  tlul_pkg::tl_h2d_t  tl_rv_dm_mem_i;
  tlul_pkg::tl_d2h_t  tl_rv_dm_mem_o;
  tlul_pkg::tl_h2d_t  tl_rv_dm_dbg_o;
  tlul_pkg::tl_d2h_t  tl_rv_dm_dbg_i;

  tlul_pkg::tl_h2d_t  tl_rv_timer_i;
  tlul_pkg::tl_d2h_t  tl_rv_timer_o;

  tlul_pkg::tl_h2d_t  tl_rv_plic_i;
  tlul_pkg::tl_d2h_t  tl_rv_plic_o;

  tlul_ibex u_core(
    .clk_i(clk_i),
    .rst_ni(sys_rst_n),
    .tl_instr_o(tl_instr_o),
    .tl_instr_i(tl_instr_i),
    .tl_data_o(tl_data_o),
    .tl_data_i(tl_data_i),
    .debug_req_i(debug_req),
    .irq_timer_i(irq_timer),
    .irq_external_i(irq_external),
    .irq_software_i(irq_software)
  );

  /* 64K byte RAM*/
  tlul_ram #(
    .MemInitFile(MemInitFile)
  ) u_ram (
    .clk_i(clk_i),
    .rst_ni(sys_rst_n),
    .tl_i(tl_ram_i),
    .tl_o(tl_ram_o)
  );

  uart u_uart(
    .clk_i(clk_i),
    .rst_ni(sys_rst_n),
    .tl_i(tl_uart_i),
    .tl_o(tl_uart_o),
    .cio_rx_i(uart_rx),
    .cio_tx_o(uart_tx),
    .cio_tx_en_o(),
    .intr_tx_watermark_o(intr_tx_watermark),
    .intr_rx_watermark_o(intr_rx_watermark),
    .intr_tx_empty_o(intr_tx_empty),
    .intr_rx_overflow_o(intr_rx_overflow),
    .intr_rx_frame_err_o(intr_rx_frame_err),
    .intr_rx_break_err_o(intr_rx_break_err),
    .intr_rx_timeout_o(intr_rx_timeout),
    .intr_rx_parity_err_o(intr_rx_parity_err)
  );

  rv_dm #(
    .NrHarts(1),
    .IdcodeValue(32'h 0000_0001)
  ) u_rv_dm (
    .clk_i(clk_i),
    .rst_ni(rst_ni),
    .hw_debug_en_i (lc_ctrl_pkg::On), // TODO connect to IO?
    .scanmode_i(lc_ctrl_pkg::Off), // TODO connect to IO
    .ndmreset_o(ndmreset),
    .dmactive_o(), // TODO connect to LED?
    .debug_req_o(debug_req),
    .unavailable_i(1'b0),

    /* Device - debug memory access port */
    .tl_d_i(tl_rv_dm_mem_i),
    .tl_d_o(tl_rv_dm_mem_o),

    /* Host - debug module to system bus access port */
    .tl_h_o(tl_rv_dm_dbg_o),
    .tl_h_i(tl_rv_dm_dbg_i),

    /* JTAG Interface */
    .jtag_req_i(jtag_req),
    .jtag_rsp_o(jtag_rsp)
  );

  rv_timer u_timer (
    .clk_i (clk_i),
    .rst_ni (sys_rst_n),

    .tl_i(tl_rv_timer_i),
    .tl_o(tl_rv_timer_o),

    .intr_timer_expired_0_0_o (irq_timer)
  );

  rv_plic u_plic (
    .clk_i(clk_i),
    .rst_ni(sys_rst_n),

    .tl_i(tl_rv_plic_i),
    .tl_o(tl_rv_plic_o),

    .intr_src_i(intr_src),
    .irq_o(irq_external),
    .irq_id_o(),
    .msip_o(irq_software)
  );

  xbar_main u_xbar(
    .clk_i(clk_i),
    .rst_ni(sys_rst_n),

    .tl_core_instr_i(tl_instr_o),
    .tl_core_instr_o(tl_instr_i),
    .tl_core_data_i(tl_data_o),
    .tl_core_data_o(tl_data_i),
    .tl_dbg_sys_bus_i(tl_rv_dm_dbg_o),
    .tl_dbg_sys_bus_o(tl_rv_dm_dbg_i),

    .tl_ram_o(tl_ram_i),
    .tl_ram_i(tl_ram_o),
    .tl_dbg_mem_o(tl_rv_dm_mem_i),
    .tl_dbg_mem_i(tl_rv_dm_mem_o),
    .tl_uart_o(tl_uart_i),
    .tl_uart_i(tl_uart_o),
    .tl_timer_o(tl_rv_timer_i),
    .tl_timer_i(tl_rv_timer_o),
    .tl_plic_o(tl_rv_plic_i),
    .tl_plic_i(tl_rv_plic_o),

    .scanmode_i(1'b0)
  );

  assign intr_src = {
    23'b0, /* Unused interrupt sources */
    intr_rx_parity_err,
    intr_rx_timeout,
    intr_rx_break_err,
    intr_rx_frame_err,
    intr_rx_overflow,
    intr_tx_empty,
    intr_rx_watermark,
    intr_tx_watermark,
    1'b0 /* interrupt source 0 needs to be always zero*/
  };

  always_comb begin
    sys_rst_n = rst_ni && !ndmreset;
  end

endmodule
