
module top_argali_basic_ulx3s (
  // Clock and Reset
  input clk_25mhz,
  input btn0,
  input ftdi_txd,
  output ftdi_rxd,
  input wifi_txd, // ESP32 U0TXD
  output wifi_rxd, // ESP32 U0RXD
  output wifi_gpio16, // ESP32 U2RXD
  input wifi_gpio17, // ESP32 U2TXD
  input gp11, // ESP32 IO26
  input gn11, // ESP32 IO25
  input gp12, // ESP32 IO33
  input gn12, // ESP32 IO32
  output gp13, // ESP32 IO35
  output gn13, // ESP32 IO34
  output led0,
  output led1,
  output led2,
  output led3,
  output led4,
  output led5,
  output led6,
  output led7
);

  /* JTAG */
  logic tck;
  logic tms;
  logic trst_n;
  logic tdi;
  logic tdo;

  logic clk;
  logic locked;
  logic rst_n;
  logic cpu_rst_n;
  logic uart_rxd;
  logic uart_txd;

  assign rst_n = btn0;

  /* FTDI UART passthrough to ESP32 UART0 */
  assign ftdi_rxd = wifi_txd;
  assign wifi_rxd = ftdi_txd;

  /* IBEX UART to ESP UART2 */
  assign uart_rxd = wifi_gpio17;
  assign wifi_gpio16 = uart_txd;

  /* IBEX JTAG from ESP32 IO */
  assign trst_n = gp11;
  assign tck = gn11;
  assign tms = gp12;
  assign tdi = gn12;
  assign gp13 = tdo;

  /* LED debugging  */
  assign led0 = cpu_rst_n;
  assign led1 = trst_n;
  assign led2 = tdi;
  assign led3 = 1'b0;
  assign led4 = 1'b0;
  assign led5 = 1'b0;
  assign led6 = !uart_txd;
  assign led7 = !uart_rxd;

  reg [1:0] locked_reg;
  always @(posedge clk)
    if (!rst_n)
      locked_reg <= 2'b00;
    else
      locked_reg <= {locked, locked_reg[1]};

  assign cpu_rst_n = &locked_reg;

  pll pll (
    .clkin(clk_25mhz),
    .clkout0(clk),
    .locked(locked)
  );

  top_argali_basic #(
    .MemInitFile("loop.hex")
  ) top (
    .clk_i(clk),
    .rst_ni(cpu_rst_n),
    .uart_rx(uart_rxd),
    .uart_tx(uart_txd),
    .tck_i(tck),
    .tms_i(tms),
    .trst_ni(trst_n),
    .tdi_i(tdi),
    .tdo_o(tdo)
  );

endmodule
