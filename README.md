# Project Argali

Project Argali is a basic experiment to implement a Ibex based RISC-V SOC with
both baremetal and RTOS (Zephyr) software support.

## Prerequisites

Before you begin, ensure you have met the following requirements:
* A linux based operating system
* A conda environment
* A zephyr development environment

Alternatively the complete environment can be installed inside a docker
container.

## Project directory setup

Project Argali and its dependencies are managed by `west`, `west` is a python
based cross-platform multi-repo management tool.

First step is to create a directory that serves as project root directory.
In this directory project Argali and its dependencies will be placed.

This newly created directory will be referred to as the root directory through
this readme file.

On a linux system following steps can be used to create a project directory
`argali` in the users home directory.

```
cd ~
mkdir argali
cd argali
git clone https://gitlab.com/dxplore/argali.git
```

Next is the setup of the development environment

## Development Environment Setup

### Native

Native setup is currently not documented but all necessary steps/dependencies
can be found in the `Dockerfile`.

### Docker West

A tool `docker-west` is provided to run all west commands inside a docker
container. Next to running the west commands inside a container the tool will
manage building the required container image.

The only requirement for this tool is a recent python version (>=3.6) and
docker.

From the project root directory run:
```
pip install --user --editable argali/tools/docker_west/
```

## Using the project

### Build and run hello world program in simulation

```
 west build -b argali_basic_sim -t fusesoc_run -d build zephyr/samples/hello_world
```

or via docker-west

```
docker-west build -b argali_basic_sim -t fusesoc_run -d build zephyr/samples/hello_world
```

### Building Argali Basic for ULX3S

```
west build -b argali_basic_ulx3s -t fusesoc_build_ulx3s_85_dxplore_systems_argali_basic -d build sw/zephyr/samples/hello_world
```

or via docker-west

```
docker-west build -b argali_basic_ulx3s -t fusesoc_build_ulx3s_85_dxplore_systems_argali_basic -d build sw/zephyr/samples/hello_world
```
