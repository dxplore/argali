# Copyright 2020 - jan.van_winkel@dxplore.eu
#
# SPDX-License-Identifier: Apache-2.0

set(OPENOCD_USE_LOAD_IMAGE YES)

include(${ZEPHYR_BASE}/boards/common/openocd.board.cmake)
