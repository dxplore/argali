/*
 * Copyright 2020 - jan.van_winkel@dxplore.eu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <init.h>
#include <sw/device/lib/dif/dif_rv_timer.h>

/* TODO get form DTS */
#define ADDR_SPACE_TIMER 0x40080000
#define CLK_FREQ 25000000 /* 25MHz */
#define TIMER_FREQ 1000000 /* 1MHz - 1us */

/**
 * Common RISC-V Timer driver is provided but it does not know how to do the
 * basic opentitan timer initialization.
 *
 * This function will take care of basic initialization, actual timer
 * configuration is handled by common driver
 */
static int opentitan_timer_init(const struct device *dev)
{
	ARG_UNUSED(dev);

	dif_rv_timer_t timer;
	dif_rv_timer_config_t timer_conf;
	dif_rv_timer_tick_params_t timer_params;
	mmio_region_t timer_base_addr;
	uint32_t hart_id = 0;
	uint32_t comp_id = 0;

	timer_base_addr.base = (volatile void *)ADDR_SPACE_TIMER;

	timer_conf.hart_count = 1;
	timer_conf.comparator_count = 1;

	if (dif_rv_timer_init(timer_base_addr, timer_conf, &timer) !=
	    kDifRvTimerOk) {
		return -EIO;
	}

	if (dif_rv_timer_approximate_tick_params(CLK_FREQ, TIMER_FREQ,
						 &timer_params) !=
	    kDifRvTimerApproximateTickParamsOk) {
		return -EIO;
	}

	if (dif_rv_timer_set_tick_params(&timer, hart_id, timer_params) !=
	    kDifRvTimerOk) {
		return -EIO;
	}

	if (dif_rv_timer_irq_enable(&timer, hart_id, comp_id,
				    kDifRvTimerEnabled) != kDifRvTimerOk) {
		return -EIO;
	}

	if (dif_rv_timer_counter_set_enabled(
		    &timer, hart_id, kDifRvTimerEnabled) != kDifRvTimerOk) {
		return -EIO;
	}

	return 0;
}

SYS_INIT(opentitan_timer_init, PRE_KERNEL_1, CONFIG_SYSTEM_CLOCK_INIT_PRIORITY);
