# Copyright 2020 - jan.van_winkel@dxplore.eu
#
# SPDX-License-Identifier: Apache-2.0
#

set(SOC_COMMON ${ZEPHYR_BASE}/soc/riscv/riscv-privilege/common)
zephyr_sources(
  ${SOC_COMMON}/idle.c
  ${SOC_COMMON}/soc_irq.S
  ${SOC_COMMON}/soc_common_irq.c
  vector.S
  opentitan_timer.c
)
zephyr_include_directories(${ZEPHYR_BASE})
