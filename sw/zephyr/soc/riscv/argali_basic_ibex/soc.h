/*
 * Copyright 2020 - jan.van_winkel@dxplore.eu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __RISCV32_ARGALI_BASIC_IBEX_SOC_H_
#define __RISCV32_ARGALI_BASIC_IBEX_SOC_H_

#include "soc/riscv/riscv-privilege/common/soc_common.h"
#include <devicetree.h>

/* Timer registers */
#define RISCV_MTIME_BASE     (DT_REG_ADDR(DT_INST(0, opentitan_timer)) + 0x104u)
#define RISCV_MTIMECMP_BASE  (DT_REG_ADDR(DT_INST(0, opentitan_timer)) + 0x10Cu)

/* lib-c hooks required RAM defined variables */
#define RISCV_RAM_BASE  DT_REG_ADDR(DT_INST(0, mmio_sram))
#define RISCV_RAM_SIZE  DT_REG_SIZE(DT_INST(0, mmio_sram))

#endif /*  __RISCV32_LITEX_VEXRISCV_SOC_H_*/
