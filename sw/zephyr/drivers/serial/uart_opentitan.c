/* Copyright 2020 - jan.van_winkel@dxplore.eu
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

#define DT_DRV_COMPAT opentitan_uart

#include <kernel.h>
#include <arch/cpu.h>
#include <drivers/uart.h>

#include <sw/device/lib/dif/dif_uart.h>

/* GCC bug we can not use (void) to suppress unused return values
 * so use a dummy if instead
 *
 * https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66425
 */
#define RVAL_NOT_USED(x) \
	do {             \
		if (x) { \
		}        \
	} while (0)

struct uart_opentitan_data {
	dif_uart_t dif_uart;
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	uart_irq_callback_user_data_t callback;
	void *cb_data;
#endif
};

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
typedef void (*irq_cfg_func_t)(void);
#endif

struct uart_opentitan_device_config {
	uintptr_t port;
	uint32_t sys_clk_freq;
	uint32_t baud_rate;
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	irq_cfg_func_t cfg_func;
#endif
};

#define DEV_CFG(dev) \
	((const struct uart_opentitan_device_config *const)(dev)->config)
#define DEV_DATA(dev) ((struct uart_opentitan_data *const)(dev)->data)
#define DEV_UART(dev) ((DEV_DATA(dev))->dif_uart)

static int uart_opentitan_init(const struct device *dev)
{
	const struct uart_opentitan_device_config *const cfg = DEV_CFG(dev);
	dif_uart_config_t uart_conf;
	dif_uart_params_t uart_params;

	uart_params.base_addr.base = (volatile void *)cfg->port;

	if (dif_uart_init(uart_params, &DEV_UART(dev)) !=
	    kDifUartOk) {
		return -EIO;
	}

	uart_conf.baudrate = cfg->baud_rate;
	uart_conf.clk_freq_hz = cfg->sys_clk_freq;
	uart_conf.parity_enable = kDifUartToggleDisabled;
	uart_conf.parity = kDifUartParityEven;

	if (dif_uart_configure(&DEV_UART(dev), uart_conf) !=
	    kDifUartConfigOk) {
		return -EIO;
	}

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	if (dif_uart_watermark_rx_set(&DEV_UART(dev), kDifUartWatermarkByte1) !=
	    kDifUartOk) {
		return -EIO;
	}

	cfg->cfg_func();
#endif

	return 0;
}

static int uart_opentitan_poll_in(const struct device *dev, unsigned char *c)
{
	size_t bytes_read;

	if (dif_uart_bytes_receive(&DEV_UART(dev), 1, c, &bytes_read) !=
	    kDifUartOk) {
		return -EINVAL;
	}

	return bytes_read == 1 ? 0 : -1;
}

static void uart_opentitan_poll_out(const struct device *dev, unsigned char c)
{
	RVAL_NOT_USED(dif_uart_byte_send_polled(&DEV_UART(dev), c));
}

static int uart_opentitan_configure(const struct device *dev,
				    const struct uart_config *cfg)
{
	return -ENOSYS;
}

static int uart_opentitan_config_get(const struct device *dev,
				     struct uart_config *cfg)
{
	return -ENOSYS;
}

#ifdef CONFIG_UART_INTERRUPT_DRIVEN

static int uart_opentitan_fifo_fill(const struct device *dev,
				    const uint8_t *tx_data, int size)
{
	size_t bytes_written;

	if (dif_uart_bytes_send(&DEV_UART(dev), tx_data, size,
				&bytes_written) != kDifUartOk) {
		return 0;
	}

	return bytes_written;
}

static int uart_opentitan_fifo_read(const struct device *dev, uint8_t *rx_data,
				    const int size)
{
	size_t bytes_read;

	if (dif_uart_bytes_receive(&DEV_UART(dev), size, rx_data,
				   &bytes_read) != kDifUartOk) {
		return 0;
	}

	RVAL_NOT_USED(dif_uart_irq_acknowledge(&DEV_UART(dev),
					       kDifUartIrqRxWatermark));

	return bytes_read;
}

static void uart_opentitan_irq_tx_enable(const struct device *dev)
{
	RVAL_NOT_USED(dif_uart_irq_set_enabled(
		&DEV_UART(dev), kDifUartIrqTxEmpty, kDifUartToggleEnabled));

	/* Interrupt is only generated when the TX buffer becomes empty.
	 * If the buffer was already empty before enabling the interrupt
	 * no interrupt is generated as such just force an interrupt so that at
	 * least uart_tx_ready is queried. Worst case there is no space in the
	 * TX buffer and a unnecessary interrupt is handled.
	 */
	RVAL_NOT_USED(
		dif_uart_irq_force(&DEV_UART(dev), kDifUartIrqTxEmpty));
}

static void uart_opentitan_irq_tx_disable(const struct device *dev)
{
	RVAL_NOT_USED(dif_uart_irq_set_enabled(
		&DEV_UART(dev), kDifUartIrqTxEmpty, kDifUartToggleDisabled));
}

static int uart_opentitan_irq_tx_ready(const struct device *dev)
{
	size_t num_bytes;
	if (dif_uart_tx_bytes_available(&DEV_UART(dev), &num_bytes) !=
	    kDifUartOk) {
		return 0;
	}

	return num_bytes != 0 ? 1 : 0;
}

static void uart_opentitan_irq_rx_enable(const struct device *dev)
{
	RVAL_NOT_USED(dif_uart_irq_set_enabled(
		&DEV_UART(dev), kDifUartIrqRxWatermark, kDifUartToggleEnabled));
}

static void uart_opentitan_irq_rx_disable(const struct device *dev)
{
	RVAL_NOT_USED(dif_uart_irq_set_enabled(
		&DEV_UART(dev), kDifUartIrqRxWatermark, kDifUartToggleDisabled));
}

static int uart_opentitan_irq_rx_ready(const struct device *dev)
{
	bool state;
	if (dif_uart_irq_is_pending(&DEV_UART(dev), kDifUartIrqRxWatermark,
				    &state) != kDifUartOk) {
		return 0;
	}

	return state ? 1 : 0;
}

static void uart_opentitan_irq_err_enable(const struct device *dev)
{
	RVAL_NOT_USED(dif_uart_irq_set_enabled(
		&DEV_UART(dev), kDifUartIrqRxParityErr, kDifUartToggleEnabled));
}

static void uart_opentitan_irq_err_disable(const struct device *dev)
{
	RVAL_NOT_USED(dif_uart_irq_set_enabled(&DEV_UART(dev),
					       kDifUartIrqRxParityErr,
					       kDifUartToggleDisabled));
}

static int uart_opentitan_irq_is_pending(const struct device *dev)
{
	if (uart_opentitan_irq_rx_ready(dev)) {
		return 1;
	}
	if (uart_opentitan_irq_tx_ready(dev)) {
		return 1;
	}
	/* TODO check error irqs */
	return 0;
}

static int uart_opentitan_irq_update(const struct device *dev)
{
	return 1;
}

static void uart_opentitan_irq_callback_set(const struct device *dev,
					    uart_irq_callback_user_data_t cb,
					    void *cb_data)
{
	struct uart_opentitan_data *data = DEV_DATA(dev);

	data->callback = cb;
	data->cb_data = cb_data;
}

static void uart_opentitan_irq_handler(const struct device *dev)
{
	struct uart_opentitan_data *data = DEV_DATA(dev);

	if (data->callback) {
		data->callback(dev, data->cb_data);
	}
}

#endif /* CONFIG_UART_INTERRUPT_DRIVEN */

static const struct uart_driver_api uart_opentitan_driver_api = {
	.poll_in = uart_opentitan_poll_in,
	.poll_out = uart_opentitan_poll_out,
	.err_check = NULL,
	.configure = uart_opentitan_configure,
	.config_get = uart_opentitan_config_get,
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	.fifo_fill = uart_opentitan_fifo_fill,
	.fifo_read = uart_opentitan_fifo_read,
	.irq_tx_enable = uart_opentitan_irq_tx_enable,
	.irq_tx_disable = uart_opentitan_irq_tx_disable,
	.irq_tx_ready = uart_opentitan_irq_tx_ready,
	.irq_tx_complete = uart_opentitan_irq_tx_ready,
	.irq_rx_enable = uart_opentitan_irq_rx_enable,
	.irq_rx_disable = uart_opentitan_irq_rx_disable,
	.irq_rx_ready = uart_opentitan_irq_rx_ready,
	.irq_err_enable = uart_opentitan_irq_err_enable,
	.irq_err_disable = uart_opentitan_irq_err_disable,
	.irq_is_pending = uart_opentitan_irq_is_pending,
	.irq_update = uart_opentitan_irq_update,
	.irq_callback_set = uart_opentitan_irq_callback_set,
#endif
};

static struct uart_opentitan_data uart_opentitan_data_0;

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
static void uart_opentitan_irq_cfg_func_0(void);
#endif

static const struct uart_opentitan_device_config uart_opentitan_dev_cfg_0 = {
	.port = DT_INST_REG_ADDR(0),
	.sys_clk_freq = DT_INST_PROP(0, clock_frequency),
	.baud_rate = DT_INST_PROP(0, current_speed),
#ifdef CONFIG_UART_INTERRUPT_DRIVEN
	.cfg_func = uart_opentitan_irq_cfg_func_0,
#endif
};

DEVICE_AND_API_INIT(uart_opentitan_0, DT_INST_LABEL(0), uart_opentitan_init,
		    &uart_opentitan_data_0, &uart_opentitan_dev_cfg_0,
		    PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE,
		    (void *)&uart_opentitan_driver_api);

#ifdef CONFIG_UART_INTERRUPT_DRIVEN
static void uart_opentitan_irq_cfg_func_0(void)
{
	IRQ_CONNECT(DT_INST_IRQ_BY_NAME(0, rx_watermark, irq),
		    1, /* TODO get prio from config */
		    uart_opentitan_irq_handler, DEVICE_GET(uart_opentitan_0),
		    0);

	IRQ_CONNECT(DT_INST_IRQ_BY_NAME(0, tx_empty, irq),
		    1, /* TODO get prio from config */
		    uart_opentitan_irq_handler, DEVICE_GET(uart_opentitan_0),
		    0);

	IRQ_CONNECT(DT_INST_IRQ_BY_NAME(0, rx_parity_err, irq),
		    1, /* TODO get prio from config */
		    uart_opentitan_irq_handler, DEVICE_GET(uart_opentitan_0),
		    0);

	irq_enable(DT_INST_IRQ_BY_NAME(0, rx_watermark, irq));
	irq_enable(DT_INST_IRQ_BY_NAME(0, tx_empty, irq));
	irq_enable(DT_INST_IRQ_BY_NAME(0, rx_parity_err, irq));
}
#endif
