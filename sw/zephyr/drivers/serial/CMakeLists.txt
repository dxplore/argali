# Copyright 2020 - jan.van_winkel@dxplore.eu
#
# SPDX-License-Identifier: Apache-2.0

# We can not use zephyr_library_amend here as it assumes that the path should be
# constructed from the module root instead of the location of the cmake file
set(ZEPHYR_CURRENT_LIBRARY drivers__serial)

zephyr_library_sources_ifdef(CONFIG_UART_OPENTITAN uart_opentitan.c)
