# Copyright 2020 - jan.van_winkel@dxplore.eu
#
# SPDX-License-Identifier: Apache-2.0

set(ARGALI_HW_BASE ${CMAKE_CURRENT_LIST_DIR}/../../../hw)

get_filename_component(FUSESOC_LIB_LOCATION_ARGALI ${ARGALI_HW_BASE} ABSOLUTE)
get_filename_component(FUSESOC_LIB_LOCATION_OPENTITAN
  ${ZEPHYR_BASE}/../../hw/opentitan ABSOLUTE
  )

function(fusesoc_build_target
    target # fusesoc build traget
    system # system to build
    )

  configure_file(${ARGALI_HW_BASE}/fusesoc.conf.in fusesoc.conf)

  string(REPLACE ":" "_" BUILD_TARGET ${system})
  string(PREPEND BUILD_TARGET "fusesoc_build_${target}_")

  add_custom_target(
    ${BUILD_TARGET}
    COMMAND fusesoc
      --config fusesoc.conf
      run
      --build-root ${CMAKE_CURRENT_BINARY_DIR}
      --build
      --target ${target}
      ${system}
    USES_TERMINAL
    )

endfunction()

function(fusesoc_run_target
    target # fusesoc build traget
    system # system to build
    )

  string(REPLACE ":" "_" BUILD_TARGET ${system})
  string(PREPEND BUILD_TARGET "fusesoc_build_${target}_")

  add_custom_target(
    fusesoc_run
    COMMAND fusesoc
      --config fusesoc.conf
      run
      --build-root ${CMAKE_CURRENT_BINARY_DIR}
      --run
      --target ${target}
      ${system}
      --run_options "-l ram,${APPLICATION_BINARY_DIR}/zephyr/${KERNEL_ELF_NAME}"
    DEPENDS ${BUILD_TARGET} ${ZEPHYR_FINAL_EXECUTABLE}
    USES_TERMINAL
    )

endfunction()

