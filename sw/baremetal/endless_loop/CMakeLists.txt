cmake_minimum_required(VERSION 3.18)

# Setup cross compilation
# TODO move to toolchain file
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR riscv)
set(CMAKE_SYSTEM_VERSION 0.0.1)

set(TOOLCHAIN_DIR "$ENV{ZEPHYR_SDK_INSTALL_DIR}/riscv64-zephyr-elf/bin")
set(CMAKE_CXX_COMPILER "${TOOLCHAIN_DIR}/riscv64-zephyr-elf-g++")
set(CMAKE_C_COMPILER "${TOOLCHAIN_DIR}/riscv64-zephyr-elf-gcc")
set(CMAKE_ASM_COMPILER "${TOOLCHAIN_DIR}/riscv64-zephyr-elf-gcc")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(ARCH_FLAGS
  "-march=rv32imc -mabi=ilp32 -mcmodel=medany -ffreestanding -g"
  )

set(CMAKE_C_FLAGS "${ARCH_FLAGS}" CACHE STRING "Base C compiler flags")

set(CMAKE_CXX_FLAGS "${ARCH_FLAGS}" CACHE STRING "Base C++ compiler flags")

set(CMAKE_ASM_FLAGS "${ARCH_FLAGS}" CACHE STRING "Base assembler flags")

set(CMAKE_EXE_LINKER_FLAGS
  "-nostdlib -nostartfiles -T ${CMAKE_CURRENT_SOURCE_DIR}/link.ld"
  CACHE STRING "Linker flags"
  )

set(BUILD_SHARED_LIBS OFF)

# END Setup cross compilation

project(endless_loop LANGUAGES C ASM)

set(ARGALI_DIR ${PROJECT_SOURCE_DIR}/../../..)
set(OPENTITAN_DIR ${ARGALI_DIR}/../hw/opentitan)
set(OPENTITAN_LIB_DIR ${OPENTITAN_DIR}/sw/device/lib)
set(OPENTITAN_LIB_BASE_DIR ${OPENTITAN_LIB_DIR}/base)
set(OPENTITAN_LIB_DIF_DIR ${OPENTITAN_LIB_DIR}/dif)
set(OPENTITAN_UART_HJSON ${OPENTITAN_DIR}/hw/ip/uart/data/uart.hjson)
set(OPENTITAN_TIMER_HJSON ${OPENTITAN_DIR}/hw/ip/rv_timer/data/rv_timer.hjson)

set(OPENTITAN_LIB_DIF_SRCS
  ${OPENTITAN_LIB_DIR}/irq.c
  ${OPENTITAN_LIB_DIF_DIR}/dif_uart.c
  ${OPENTITAN_LIB_DIF_DIR}/dif_rv_timer.c
  )

set(OPENTITAN_LIB_BASE_SRCS
  ${OPENTITAN_LIB_BASE_DIR}/bitfield.c
  ${OPENTITAN_LIB_BASE_DIR}/mmio.c
  ${OPENTITAN_LIB_BASE_DIR}/memory.c
  )

set(GENERATED_INCLUDES_DIR ${CMAKE_BINARY_DIR}/generated)

macro(genregs HJSON OUT)
  add_custom_command(
    OUTPUT ${GENERATED_INCLUDES_DIR}/${OUT}
    COMMAND ${OPENTITAN_DIR}/util/regtool.py -D -o
    ${GENERATED_INCLUDES_DIR}/${OUT} ${HJSON}
    DEPENDS ${HJSON}
    COMMENT "Generating ${OUT}"
  )
endmacro() # genregs

genregs(${OPENTITAN_UART_HJSON} uart_regs.h)
genregs(${OPENTITAN_TIMER_HJSON} rv_timer_regs.h)

set(OPENTITAN_REG_HDRS
  ${GENERATED_INCLUDES_DIR}/uart_regs.h
  ${GENERATED_INCLUDES_DIR}/rv_timer_regs.h
  )
add_library(opentitan_reg_headers INTERFACE)
target_sources(opentitan_reg_headers INTERFACE ${OPENTITAN_REG_HDRS})

add_library(opentitan_base ${OPENTITAN_LIB_BASE_SRCS})
target_include_directories(opentitan_base PRIVATE ${OPENTITAN_DIR})

add_library(opentitan_dif ${OPENTITAN_LIB_DIF_SRCS})
# depend on libgcc to support div
target_link_libraries(opentitan_dif PRIVATE opentitan_base gcc)
target_link_libraries(opentitan_dif PRIVATE opentitan_reg_headers)
target_include_directories(opentitan_dif PRIVATE
  ${OPENTITAN_DIR}
  ${GENERATED_INCLUDES_DIR}
  )

add_executable(endless_loop crt0.S endless_loop.c)
target_link_libraries(endless_loop PRIVATE opentitan_dif)
target_include_directories(endless_loop PRIVATE ${OPENTITAN_DIR})
