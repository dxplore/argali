#include <sw/device/lib/irq.h>
#include <sw/device/lib/base/mmio.h>
#include <sw/device/lib/dif/dif_uart.h>
#include <sw/device/lib/dif/dif_rv_timer.h>

#define ADDR_SPACE_RAM 0x10000000
#define ADDR_SPACE_UART 0x40000000
#define ADDR_SPACE_TIMER 0x40080000
#define to_hex(i) (((i & 0xf) > 9) ? (i & 0xf) - 10 + 'A' : (i & 0xf) + '0')

#define BAUDRATE 921600
#define CLK_FREQ 25000000 /* 25MHz */
#define TIMER_FREQ 1000000 /* 1MHz - 1us */
#define TICK_PERIOD 1000 /* 1000 timer increments (1ms)*/
#define PRINT_PERIOD 11 /* in ticks (11ms) */

/* GCC bug we can not use (void) to suppress unused return values
 * so use a dummy if instead
 *
 * https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66425
 */
#define RVAL_NOT_USED(x) \
	do {             \
		if (x) { \
		}        \
	} while (0)

static dif_rv_timer_t timer;
static uint64_t tick_cnt = 0;

void set_next_tick()
{
	uint64_t time;
	RVAL_NOT_USED(dif_rv_timer_counter_read(&timer, 0, &time));
	/* TODO correctly handle overflow
	 * Code bellow could lead to spurious interrupts once counter approaches
	 * 2^64
	 */
	RVAL_NOT_USED(dif_rv_timer_arm(&timer, 0, 0, time + TICK_PERIOD));
}

void handle_timer_irq() __attribute__((interrupt));

void handle_timer_irq()
{
	set_next_tick();
	++tick_cnt;
	RVAL_NOT_USED(dif_rv_timer_irq_clear(&timer, 0, 0));
}

int main(int argc, char *argv[])
{
	dif_uart_t uart;
	dif_uart_config_t uart_conf;
	dif_rv_timer_config_t timer_conf;
	dif_rv_timer_tick_params_t timer_params;
	mmio_region_t uart_base_addr;
	mmio_region_t timer_base_addr;

	uart_base_addr.base = (volatile void *)ADDR_SPACE_UART;

	uart_conf.baudrate = BAUDRATE;
	uart_conf.clk_freq_hz = CLK_FREQ;
	uart_conf.parity_enable = kDifUartDisable;
	uart_conf.parity = kDifUartParityEven;

	RVAL_NOT_USED(dif_uart_init(uart_base_addr, &uart_conf, &uart));

	timer_base_addr.base = (volatile void *)ADDR_SPACE_TIMER;

	timer_conf.hart_count = 1;
	timer_conf.comparator_count = 1;

	irq_global_ctrl(true);
	irq_timer_ctrl(true);

	RVAL_NOT_USED(dif_rv_timer_init(timer_base_addr, timer_conf, &timer));

	RVAL_NOT_USED(dif_rv_timer_approximate_tick_params(CLK_FREQ, TIMER_FREQ,
							   &timer_params));
	RVAL_NOT_USED(dif_rv_timer_set_tick_params(&timer, 0, timer_params));

	RVAL_NOT_USED(
		dif_rv_timer_irq_enable(&timer, 0, 0, kDifRvTimerEnabled));

	set_next_tick();
	RVAL_NOT_USED(dif_rv_timer_counter_set_enabled(&timer, 0,
						       kDifRvTimerEnabled));

	uint32_t x = 0;
	int idx;
	uint64_t prev_tick_cnt = tick_cnt;
	while (1) {
		if ((tick_cnt - prev_tick_cnt) >= PRINT_PERIOD) {
			prev_tick_cnt = tick_cnt;
			idx = 32 - 4;
			while (idx >= 0) {
				RVAL_NOT_USED(dif_uart_byte_send_polled(
					&uart, to_hex(x >> idx)));
				idx -= 4;
			}
			RVAL_NOT_USED(dif_uart_byte_send_polled(&uart, '\r'));
			RVAL_NOT_USED(dif_uart_byte_send_polled(&uart, '\n'));
		}
		++x;
	}
	return 0;
}
