# Copyright 2020 - jan.van_winkel@dxplore.eu
#
# SPDX-License-Identifier: Apache-2.0

ARG ZEPHYR_TOOLCHAIN_VERSION=0.11.4
ARG ZEPHYR_TOOLCHAIN_PATH=/opt/zephyr-sdk-${ZEPHYR_TOOLCHAIN_VERSION}

################################################################################
#
# Container for installing zephyr sdk
#
# The Zephyr SDK installer does not use tools installed inside a conda
# environment and rely on the tools installed in the system. As we want to
# minimize the tools installed in the system and use only tools installed inside
# the conda environment create a dedicated SDK install container.
#
# In the consecutive Argali base image we just will copy the installed SDK from
# this container.
#
################################################################################

FROM ubuntu:20.04 AS zephyr-sdk

RUN apt-get update && \
    apt-get install --no-install-recommends --yes \
        ca-certificates \
        wget \
        python3 \
        xz-utils \
    && rm -rf /var/lib/apt/lists/*

ARG ZEPHYR_TOOLCHAIN_VERSION
ARG ZEPHYR_TOOLCHAIN_PATH
ARG ZEPHYR_RISCV_TOOLCHAIN_URL=https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v${ZEPHYR_TOOLCHAIN_VERSION}/zephyr-toolchain-riscv64-${ZEPHYR_TOOLCHAIN_VERSION}-setup.run

RUN wget --quiet ${ZEPHYR_RISCV_TOOLCHAIN_URL} -O ~/riscv_toolchain.run && \
    chmod +x ~/riscv_toolchain.run && \
    ~/riscv_toolchain.run -- -y -rc \
        -d  ${ZEPHYR_TOOLCHAIN_PATH} && \
    rm ~/riscv_toolchain.run

################################################################################
#
# Base Argali container
#
# A container containing all necessary dependency to do Argali development
#
# Depends on zephyr-sdk container
#
################################################################################


FROM ubuntu:20.04 AS argali-base

ARG ZEPHYR_TOOLCHAIN_PATH

COPY --from=zephyr-sdk ${ZEPHYR_TOOLCHAIN_PATH} ${ZEPHYR_TOOLCHAIN_PATH}

ARG CONDA_DIR=/opt/conda
ARG ARGALI_DIR=/argali

ENV ENV_PREFIX=/opt/conda_env

ENV LANGUAGE="en"
ENV LC_ALL=C.UTF-8
ENV LC_MESSAGES=C.UTF-8
ENV LANG=C.UTF-8

ENV PATH=${CONDA_DIR}/bin:$PATH

SHELL [ "/bin/bash", "--login", "-c" ]

RUN apt-get update && \
    apt-get install --no-install-recommends --yes \
        ca-certificates \
        screen \
        wget \
    && rm -rf /var/lib/apt/lists/*


ARG MINICONDA_SCRIPT=Miniconda3-py38_4.8.3-Linux-x86_64.sh
ARG MINICONDA_URL=https://repo.anaconda.com/miniconda

RUN wget --quiet ${MINICONDA_URL}/${MINICONDA_SCRIPT} -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p ${CONDA_DIR} && \
    rm ~/miniconda.sh && \
    ln -s ${CONDA_DIR}/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    ${CONDA_DIR}/bin/conda init bash

COPY environment.yml \
    requirements.txt \
    /tmp/

ENV PERL5LIB=/opt/conda_env/lib/5.26.2

RUN conda update -n base -c defaults conda && \
    conda env create \
        --prefix ${ENV_PREFIX} \
        --file /tmp/environment.yml \
        --force && \
    rm /tmp/environment.yml /tmp/requirements.txt && \
    conda install --prefix ${ENV_PREFIX} perl && \
    conda clean --all --yes && \
    echo "conda activate ${ENV_PREFIX}" >> ~/.bashrc

WORKDIR /workdir

COPY tools/entrypoint.sh /usr/local/bin/
RUN chmod a+x /usr/local/bin/entrypoint.sh

# For some reason yosys looks in the wrong directory for its installed shared
# files. So just copy everything form share/yosys to share/ as a workaround
RUN cp -r ${ENV_PREFIX}/share/yosys/* ${ENV_PREFIX}/share/

################################################################################
#
# Development Argali container
#
# A container based on argali-base with a newly created user.
#
# Depends on argali-base
#
################################################################################

FROM argali-base AS argali-dev

ARG USER
ARG GROUP
ARG UID
ARG GID
ARG HOME=/home/${USER}

RUN groupadd \
        --gid ${GID} \
        ${GROUP} && \
    useradd \
        --uid ${UID} \
        --gid ${GID} \
        --create-home \
        --home ${HOME} \
        --shell /bin/bash \
        ${USER} && \
    chown -R ${USER}:${GROUP} /workdir

USER ${USER}

RUN conda init bash && \
    conda config --set changeps1 False && \
    echo "conda activate ${ENV_PREFIX}" >> ~/.bashrc
