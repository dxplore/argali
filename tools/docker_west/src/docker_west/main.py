# Copyright 2020 Jan Van Winkel <jan.van_winkel@dxplore.eu>
#
# SPDX-License-Identifier: Apache-2.0
'''
Main file for docker_west
'''

from subprocess import Popen, PIPE, STDOUT
import argparse
import getpass
import grp
import os
import pathlib
import random
import signal
import string
import sys

import docker


class SigIntHandler:
    '''
    SIGINT (CTRL+C) handler
    '''

    def __init__(self):
        self.container = None
        self.count = 0

    def __call__(self, signo, frame):
        rval = self.count

        if self.container and (self.count == 0):
            self.count = 1
            print("\nStopping docker container...\n")
            print("Press CTRL+C once more to terminate python script")
            print("WARNING: This will leave the running container in " +
                  "an unknown state")
            self.container.kill(signal.SIGINT)
            rval = self.container.wait()["StatusCode"]
            self.container = None

        sys.exit(rval)

    def set_container(self, container):
        '''
        Associate a container with the signal handler
        '''
        self.container = container

    def clear_container(self):
        '''
        Clear associated container
        '''
        self.set_container(None)


def get_dockerfile_sha(repo_root):
    '''
    Get short SHA for Dockerfile in the repo
    '''
    cmd = f"git -C {repo_root} rev-parse --short HEAD:Dockerfile"
    pipe = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE,
                 stderr=STDOUT, close_fds=True)
    return pipe.communicate()[0].decode().strip()


def get_image_name(repo_root):
    '''
    Get docker image name based on the repo name, user name and SHA of the
    Dockerfile in the repo
    '''
    username = getpass.getuser()
    name = repo_root.name
    sha = get_dockerfile_sha(repo_root)
    return f"{username}/{name}:{sha}"


def generate_container_name(repo_root):
    '''
    Generate a unique container name based on the repo name, user name
    '''
    username = getpass.getuser()
    name = repo_root.name
    uni = ''.join(random.choices(string.ascii_uppercase + string.digits, k=4))
    return f"{username}_{name}_{uni}"


def has_docker_image(repo_root):
    '''
    Check if docker image exists for the user executing the script in the given
    repo
    '''
    image_name = get_image_name(repo_root)
    client = docker.from_env()
    return len(client.images.list(name=image_name)) != 0


def build_docker_image(repo_root):
    '''
    Build new docker image
    '''
    user = getpass.getuser()
    uid = os.getuid()
    gid = os.getgid()
    group = grp.getgrgid(gid)[0]

    print("Building docker image, this can take a while ...")
    image_name = get_image_name(repo_root)
    client = docker.from_env()
    client.images.build(path=str(repo_root), tag=image_name, buildargs={
        "USER": user,
        "GROUP": group,
        "UID": str(uid),
        "GID": str(gid)
    })


def run_cmd_in_docker_conatiner(name, sigint_handler, repo_root, cmd):
    '''
    Execute the given command inside a container
    '''
    west_root = repo_root / ".."
    image_name = get_image_name(repo_root)
    if not name:
        name = generate_container_name(repo_root)
    client = docker.from_env()
    container = client.containers.run(
        image=image_name,
        command=cmd,
        name=name,
        entrypoint=["entrypoint.sh"],
        hostname="argali",
        remove=True,
        detach=True,
        volumes={str(west_root): {'bind': str(west_root), 'mode': 'rw'}
                 },
        environment=[f"CCACHE_DIR={str(west_root)}/ccache"],
        working_dir=str(west_root)
    )
    sigint_handler.set_container(container)

    for log in container.logs(stream=True):
        print(f"{log.decode()}", end='')

    return container.wait()["StatusCode"]


def main():
    '''
    docker-west main entry point
    '''

    repo_root = pathlib.Path(__file__) / ".." / ".." / ".." / ".." / ".."
    repo_root = repo_root.resolve()

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-h', '--help', action='store_true',
                        help='show this help message and exit')
    parser.add_argument('-r', '--rebuild', action='store_true',
                        help='rebuild container before '
                        + 'executing anything else')

    if "--" in sys.argv[1:]:
        idx = sys.argv.index("--")
        args = parser.parse_args(sys.argv[1:idx])
        remainder = sys.argv[idx + 1:]
    else:
        args, remainder = parser.parse_known_args()

    if args.rebuild:
        build_docker_image(repo_root)

    sigint_handler = SigIntHandler()
    signal.signal(signal.SIGINT, sigint_handler)

    if args.help:
        parser.print_help()
        if has_docker_image(repo_root):
            print('\n\n')
            run_cmd_in_docker_conatiner(None, sigint_handler, repo_root,
                                        "west --help")
        sys.exit(0)

    if not has_docker_image(repo_root):
        build_docker_image(repo_root)

    west_args = " ".join(remainder)
    return run_cmd_in_docker_conatiner(None, sigint_handler, repo_root,
                                       f"west {west_args}")
