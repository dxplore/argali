# Copyright 2020 Jan Van Winkel <jan.van_winkel@dxplore.eu>
#
# SPDX-License-Identifier: Apache-2.0

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="docker-west",
    version="0.0.1",
    author="Jan Van Winkel",
    author_email="jan.van_winkel@dxplore.eu",
    description="Docker wrapper tool for west",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/dxplore/argali",
    packages=setuptools.find_packages(where='src'),
    package_dir={'': 'src'},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX :: Linux',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows'
    ],
    install_requires=[
        'docker',
        'setuptools',
    ],
    python_requires='>=3.6',
    entry_points={'console_scripts': ('docker-west = docker_west.main:main',)},
)
