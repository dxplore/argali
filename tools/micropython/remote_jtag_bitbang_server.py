# Copyright 2020 Jan Van Winkel <jan.van_winkel@dxplore.eu>
#
# SPDX-License-Identifier: Apache-2.0
'''
Micropython OpenOCD remote JTAG bitbang server

This module requires micropython version >= 1.13 and ulogging.
Ulogging can be installed as follows from the repl prompt:
    import upip
    upip.install('micropython-ulogging')


From the OpenOCD documentation (doc/manual/jtag/drivers/remote_bitbang.txt):

The remote_bitbang JTAG driver is used to drive JTAG from a remote process. The
remote_bitbang driver communicates via TCP or UNIX sockets with some remote
process using an ASCII encoding of the bitbang interface. The remote process
presumably then drives the JTAG however it pleases. The remote process should
act as a server, listening for connections from the openocd remote_bitbang
driver.

The remote bitbang driver is useful for debugging software running on
processors which are being simulated.

The bitbang interface consists of the following functions.

blink on
  Blink a light somewhere. The argument on is either 1 or 0.

read
   Sample the value of tdo.

write tck tms tdi
  Set the value of tck, tms, and tdi.

reset trst srst
  Set the value of trst, srst.

An additional function, quit, is added to the remote_bitbang interface to
indicate there will be no more requests and the connection with the remote
driver should be closed.

These five functions are encoded in ASCII by assigning a single character to
each possible request. The assignments are:

        B - Blink on
        b - Blink off
        R - Read request
        Q - Quit request
        0 - Write 0 0 0
        1 - Write 0 0 1
        2 - Write 0 1 0
        3 - Write 0 1 1
        4 - Write 1 0 0
        5 - Write 1 0 1
        6 - Write 1 1 0
        7 - Write 1 1 1
        r - Reset 0 0
        s - Reset 0 1
        t - Reset 1 0
        u - Reset 1 1

The read response is encoded in ASCII as either digit 0 or 1.

'''


from machine import Pin
from time import sleep_ms
import uasyncio as asyncio
import ulogging as logging


class RemoteJtagBitbangServer:

    def __init__(self,
                 led_pin_id=None,
                 tck_pin_id=None,
                 tms_pin_id=None,
                 tdi_pin_id=None,
                 tdo_pin_id=None,
                 trst_n_pin_id=None,
                 srst_n_pin_id=None,
                 discard_unknown_cmds=False):

        self._log = logging.getLogger("RemoteJtagBitbangServer")

        self._discard_unknown = discard_unknown_cmds

        self._led = False
        self._tck = False
        self._tms = False
        self._tdi = False
        self._tdo = False
        self._trst_n = False
        self._srst_n = False

        self._led_pin = Pin(led_pin_id, Pin.OUT) if led_pin_id else None
        self._tck_pin = Pin(tck_pin_id, Pin.OUT) if tck_pin_id else None
        self._tms_pin = Pin(tms_pin_id, Pin.OUT) if tms_pin_id else None
        self._tdi_pin = Pin(tdi_pin_id, Pin.OUT) if tdi_pin_id else None
        self._tdo_pin = Pin(tdo_pin_id, Pin.IN) if tdo_pin_id else None
        self._trst_n_pin = Pin(
            trst_n_pin_id, Pin.OUT) if trst_n_pin_id else None
        self._srst_n_pin = Pin(
            srst_n_pin_id, Pin.OUT) if srst_n_pin_id else None

        if self._trst_n_pin:
            self._trst_n_pin(False)
        if self._srst_n_pin:
            self._srst_n_pin(False)

        sleep_ms(250)

        if self._srst_n_pin:
            self._srst_n_pin(True)
            self._srst_n = True

    def __del__(self):
        led = self._led_pin
        tck = self._tck_pin
        tms = self._tms_pin
        tdi = self._tdi_pin
        tdo = self._tdo_pin
        trst = self._trst_n_pin
        srst = self._srst_n_pin

        self._led_pin = None
        self._tck_pin = None
        self._tms_pin = None
        self._tdi_pin = None
        self._tdo_pin = None
        self._trst_n_pin = None
        self._srst_n_pin = None

        led.mode(Pin.IN)
        tck.mode(Pin.IN)
        tms.mode(Pin.IN)
        tdi.mode(Pin.IN)
        trst.mode(Pin.IN)
        srst.mode(Pin.IN)
        del led
        del tck
        del tms
        del tdi
        del tdo
        del trst
        del srst

        self._log.info("OpenOCD remote JTAG bitbang server terminated")

    async def __call__(self, reader, writer):

        addr = writer.get_extra_info('peername')
        self._log.info("Client connected to OpenOCD remote " +
                       "JTAG bitbang from " +
                       "{addr!r}".format(addr=addr))

        while (True):
            data = await reader.read(1)
            cmd = data.decode()

            if not len(cmd):
                self._log.info("Connection closed by client")
                break

            if(cmd == 'B'):
                self._log.debug("Blink on")
                await self.set_led(True)
            elif(cmd == 'b'):
                self._log.debug("Blink off")
                await self.set_led(False)
            elif(cmd == 'R'):
                await self.sample_tdo()
                self._log.debug("Read request: {}".format(self._tdo))
                writer.write(b'1' if self._tdo else b'0')
                await writer.drain()
            elif(cmd == 'Q'):
                self._log.info("Quit, closing connection")
                break
            elif(cmd == '0'):
                self._log.debug("Write 0 0 0")
                await self.write_tms(False)
                await self.write_tdi(False)
                await self.write_tck(False)
            elif(cmd == '1'):
                self._log.debug("Write 0 0 1")
                await self.write_tms(False)
                await self.write_tdi(True)
                await self.write_tck(False)
            elif(cmd == '2'):
                self._log.debug("Write 0 1 0")
                await self.write_tms(True)
                await self.write_tdi(False)
                await self.write_tck(False)
            elif(cmd == '3'):
                self._log.debug("Write 0 1 1")
                await self.write_tms(True)
                await self.write_tdi(True)
                await self.write_tck(False)
            elif(cmd == '4'):
                self._log.debug("Write 1 0 0")
                await self.write_tms(False)
                await self.write_tdi(False)
                await self.write_tck(True)
            elif(cmd == '5'):
                self._log.debug("Write 1 0 1")
                await self.write_tms(False)
                await self.write_tdi(True)
                await self.write_tck(True)
            elif(cmd == '6'):
                self._log.debug("Write 1 1 0")
                await self.write_tms(True)
                await self.write_tdi(False)
                await self.write_tck(True)
            elif(cmd == '7'):
                self._log.debug("Write 1 1 1")
                await self.write_tms(True)
                await self.write_tdi(True)
                await self.write_tck(True)
            elif(cmd == 'r'):
                self._log.debug("Reset 0 0")
                await self.write_trst(False)
                await self.write_srst(False)
            elif(cmd == 's'):
                self._log.debug("Reset 0 1")
                await self.write_trst(False)
                await self.write_srst(True)
            elif(cmd == 't'):
                self._log.debug("Reset 1 0")
                await self.write_trst(True)
                await self.write_srst(False)
            elif(cmd == 'u'):
                self._log.debug("Reset 1 1")
                await self.write_trst(True)
                await self.write_srst(True)
            elif(not self._discard_unknown):
                self._log.warning("Unknown command received " +
                                  "({cmd}), ".format(cmd=cmd) +
                                  "closing connection")

        writer.close()
        await writer.wait_closed()

    async def set_led(self, led):
        if self._led == led:
            return
        self._led = led
        if self._led_pin:
            self._led_pin(self._led)

    async def write_tck(self, tck):
        if self._tck == tck:
            return
        self._tck = tck
        if self._tck_pin:
            self._tck_pin(self._tck)

    async def write_tms(self, tms):
        if self._tms == tms:
            return
        self._tms = tms
        if self._tms_pin:
            self._tms_pin(self._tms)

    async def write_tdi(self, tdi):
        if self._tdi == tdi:
            return
        self._tdi = tdi
        if self._tdi_pin:
            self._tdi_pin(self._tdi)

    async def write_trst(self, trst):
        trst_n = not trst
        if self._trst_n == trst_n:
            return
        self._trst_n = trst_n
        if self._trst_n_pin:
            self._trst_n_pin(self._trst_n)

    async def write_srst(self, srst):
        srst_n = not srst
        if self._srst_n == srst_n:
            return
        self._srst_n = srst_n
        if self._srst_n_pin:
            self._srst_n_pin(self._srst_n)

    async def sample_tdo(self):
        self._tdo = self._tdo_pin() if self._tdo_pin else False

    async def stop(self):
        self.server.close()
        await self.server.wait_closed()


def run_remote_jtag_bitbang_server(led_pin_id=None,
                                   tck_pin_id=None,
                                   tms_pin_id=None,
                                   tdi_pin_id=None,
                                   tdo_pin_id=None,
                                   trst_n_pin_id=None,
                                   srst_n_pin_id=None,
                                   host="0.0.0.0",
                                   port=1234,
                                   debug=False
                                   ):
    logging.basicConfig()
    log = logging.getLogger("RemoteJtagBitbangServer")
    if debug:
        log.setLevel(logging.DEBUG)
    server = RemoteJtagBitbangServer(discard_unknown_cmds=debug,
                                     led_pin_id=led_pin_id,
                                     tck_pin_id=tck_pin_id,
                                     tms_pin_id=tms_pin_id,
                                     tdi_pin_id=tdi_pin_id,
                                     tdo_pin_id=tdo_pin_id,
                                     trst_n_pin_id=trst_n_pin_id,
                                     srst_n_pin_id=srst_n_pin_id)
    async_server = asyncio.start_server(server,
                                        host=host,
                                        port=port,
                                        backlog=1)
    loop = asyncio.get_event_loop()
    loop.create_task(async_server)
    log.info("Running OpenOCD remote JTAG bitbang server " +
             "on {host}:{port}".format(host=host, port=port))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        loop.close()
