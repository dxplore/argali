# Copyright 2020 Jan Van Winkel <jan.van_winkel@dxplore.eu>
#
# SPDX-License-Identifier: Apache-2.0
'''
Main file for ulx3s_bitloader
'''

import argparse
import ftplib


def main():
    '''
    ulx3s-bitloader main entry point
    '''

    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--ftp',
                        help='Upload bit file over FTP to given IP address')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('bitfile', help='Path to bit file')
    args = parser.parse_args()

    if args.ftp:
        print(f"Programming ECP5 FPGA over FTP on ULX3S @ {args.ftp}")
        with open(args.bitfile, 'rb') as f:
            with ftplib.FTP(args.ftp) as ftp:
                ftp.set_pasv(False)
                if args.verbose:
                    ftp.set_debuglevel(1)
                ftp.login()
                ftp.storbinary("STOR /fpga", f)
